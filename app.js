const express = require('express')
const app = express()
const mongoose = require('mongoose')
const bodyParser= require('body-parser')
const postsRoute = require('./routes/posts')
require('dotenv/config')

app.use(bodyParser.json())
app.use('/posts', postsRoute)

mongoose.connect(process.env.MONGO_URI, { useNewUrlParser: true })
  .then(() => console.info(new Date(), 'Database connected.'))
  .catch(err => console.error(new Date(), 'Error connecting to database: ', err))

app.listen(3000, () => console.info(new Date(), 'Server running on port 3000.'))