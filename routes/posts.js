const express = require('express')
const router = express.Router()
const Post = require('../models/Post')

router.get('/', async (req, res) => {
  try {
    const posts = await Post.find()
    console.log(new Date(), '[POSTS] Showing posts list.')
    res.send(posts)
  } catch(err) {
    console.error(new Date(), '[POSTS] Error showing posts: ', err)
    res.send({ error: err })
  }
})

router.post('/', async (req, res) => {
  const post = new Post({
    title: req.body.title,
    content: req.body.content
  })

  try {
    const saved = await post.save()
    console.log(new Date(), '[POSTS] Post saved.')
    res.send(saved)
  } catch (err) {
    console.error(new Date(), '[POSTS] Error saving post: ', err)
    res.send({ error: err })
  }
})

module.exports = router