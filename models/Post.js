const mongoose = require('mongoose')

const PostSchema = mongoose.Schema({
  title: String,
  author: String,
  content: String,
  createdAt: {
    type: Date,
    default: Date.now
  }
})

module.exports = mongoose.model('Posts', PostSchema)